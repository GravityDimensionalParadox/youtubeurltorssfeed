#!/bin/bash

touch YTchannelRSSFeeds.txt
cat $1 | while read line; do
        printf "https://www.youtube.com/feeds/videos.xml?channel_id=" >> YTchannelRSSFeeds.txt
        curl $line -s | grep -o -P 'meta itemprop="channelId" content=.{0,26}' | cut -c 36-59 >> YTchannelRSSFeeds.txt
        printf '\n' >> YTchannelRSS.txt
done
