#!/bin/bash

printf "https://www.youtube.com/feeds/videos.xml?channel_id="
curl $1 -s | grep -o -P 'meta itemprop="channelId" content=.{0,26}' | cut -c 36-59
